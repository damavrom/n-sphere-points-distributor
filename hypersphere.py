import random
import math

norm = lambda p: math.sqrt(sum(map(lambda cord: cord**2, p)))
def normalize(p):
    p_norm = norm(p)
    return tuple(map(lambda cord: cord/p_norm, p))

def random_sphere_points(n, dims = 3):
    points = []
    cnt = 0
    while len(points) < n:
        p = tuple([random.random()*2-1 for j in range(dims)])
        p_norm = norm(p)
        if p_norm <= 1:
            points.append(tuple(map(lambda cord: cord/p_norm, p)))
    return points

dist = lambda p1, p2: math.sqrt(sum(map(
    lambda cord: (cord[0]-cord[1])**2,
    zip(p1, p2)
)))

def make_uniform(points):
    points_n = []
    dims = len(points[0])
    for indx1 in range(len(points)):
        p1 = points[indx1]
        total_f = tuple(map(lambda cord: 0, p1))
        for indx2 in range(len(points)):
            if indx1 != indx2:
                p2 = points[indx2]
                d = dist(p1, p2)
                f = tuple(map(lambda x: (x[0]-x[1])/d**(dims-1), zip(p1, p2)))
                total_f = tuple(map(lambda x: sum(x), zip(f, total_f)))
        p1_n = tuple(map(lambda x: x[0]+x[1], zip(p1, total_f)))
        points_n.append(normalize(p1_n))
    return points_n

if __name__ == "__main__":
    n = 500
    sphere = random_sphere_points(n, dims = 3)
    with open("random_sphere", "w") as fl:
        fl.write("\n".join(map(
            lambda p: "\t".join(map(lambda cord: f"{cord}", p)),
            sphere
        )))
        fl.write("\n")

    its = 100
    for i in range(its):
        print("it:", i)
        sphere = make_uniform(sphere)
        sphere.sort(key = lambda p: dist(p, (1, 0, 0, 0)))
        with open("sphere", "w") as fl:
            fl.write("\n".join(map(
                lambda p: "\t".join(map(lambda cord: f"{cord}", p)),
                sphere
            )))
            fl.write("\n")
