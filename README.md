# n-sphere points distributor
Sooner or later everybody will need uniformly distributed points on a
sphere. But also on a n-sphere. There doesn't seem to be a standard
method for doing this. There doesn't seem to be code on the Internet for
doing this. So I did the job and coded a program for it.

It works like a repulsion simulator. First, it chooses random points on
an n-sphere. Then vertices push other vertices away in a similar way
electrons push away other electrons in an atom.

### features
 - python3
 - super simple code with no comments
 - no stupid dependencies
 - dynamic number of points
 - dynamic number of dimensions
 - $`O(n^2)`$ iteration cost

### examples
Here there are 500 randomly positioned points on a sphere:

![random 3]

Here they are after some iterations:

![uniform 3]

Here there are 500 randomly positioned points on a 3-sphere projected on three
dimensions:

![random 4]

Here they are after some iterations:

![uniform 4]

### note
The beauty of this program is that it can work very fast for any number
of dimensions. The _repulsion force_ for each point is computed
according to the distance between points. In real life -the 3D world
that we live in,- the gravitational and Coulomb's forces are
proportional to the square of distance. With a lower degree polynomial
of the distance, the simulator runs slow. With a higher degree
polynomial the simulator becomes unstable. But this applies only for
three dimensions. From tweaking around, I discovered that for four
dimensions, the forces are better computed proportionally to the cube of
distance. For five to the fourth power, and so on. This discovery was
made by experimenting; I don't know the theory of Coulomb on any number
of dimensions.

Still, the more dimension, the more iterations are needed to reach a
stable state. For two dimensions, a couple of iterations are only
needed. For three, a couple more. For six, around 25. For eight, 40.

[uniform 3]: examples/uniform_3_500.png
[random 3]: examples/random_3_500.png
[uniform 4]: examples/uniform_4_500.png
[random 4]: examples/random_4_500.png
